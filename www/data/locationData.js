var LocationData = [
    [
        "강남역/신논현/역삼",
        "가로수길/압구정/논현",
        "청담",
        "삼성/선릉/대치",
        "홍대/합정/상수",
        "신촌/이대/공덕",
        "상암/은평",
        "명동/시청/중구",
        "종로/광화문/을지로",
        "이태원/한남",
        "잠실/신천/석촌",
        "방이/가락/문정",
        "강동/천호/길동",
        "서초/방배/양재",
        "강서/양천/목동",
        "관악/동작/사당",
        "영등포/구로",
        "대학로/수유/미아",
        "노원/도봉",
        "성동/동대문",
        "건대/구의/중랑"
    ],
    [
        "파주/고양/김포",
        "수원",
        "성남",
        "부천",
        "용인",
        "의왕/군포/안산",
        "구리/남양주/하남",
        "화성/오산",
        "의정부/양주/동두천",
        "안양/과천",
        "평택/안성",
        "시흥/광명",
        "광주/이천"
    ],
    [
        "부평구",
        "남동구/남구",
        "서구/계양구",
        "연수구/중구"
    ]
];