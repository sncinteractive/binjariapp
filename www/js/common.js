var popupCallback;
var popupConfirmCallback;
var popupCancelCallback;
var currentLocationTabIndex = 0;
var currentLocationIndex = 1;
var gcmId = "";

var DEF_LOC_LAT = 37.5220135;
var DEF_LOC_LNG = 127.0401377;

var SERVER_HOST_OFFICE = "http://192.168.0.25";
var SERVER_HOST_HOME = "http://192.168.219.102";
var SERVER_HOST_DEV = "http://dev.sncinteractive.com";
var SERVER_HOST_REAL = "http://binjari.co.kr:8080";
var SERVER_HOST_FOR_WEB = "";

var SERVER_HOST = SERVER_HOST_REAL;
var SERVER_HOST_API = SERVER_HOST + "/api";

var IMAGE_SERVER_HOST = SERVER_HOST_REAL;
var IFRME_SERVER_HOST = SERVER_HOST_REAL;

function isPopupShowing() {
    var isShowing = false;
    $.each($(".layer_wrap"), function(_key, _val) {
        if ($(this).css("display") != "none") {
            isShowing = true;
            $(this).hide();
        }
    });

    return isShowing;
}

function showPopup(_content) {
    $("#layer > .pop_posi > p").html(_content);
    $("#layer").show();
}

function hidePopup() {
    $("#layer").hide();

    if (popupCallback) {
        popupCallback();
        popupCallback = null;
    }
}

function showConfirmPopup(_content) {
    $("#layerConfirm > .pop_posi > p").html(_content);
    $("#layerConfirm").show();
}

function hideConfirmPopup(_ret) {
    $("#layerConfirm").hide();

    if (popupConfirmCallback) {
        if (_ret === true) {
            popupConfirmCallback(true);
        } else {
            popupConfirmCallback(false);
        }

        popupConfirmCallback = null;
    }
}

function removeAllMemberData() {
    removeStorageData("token");
    removeStorageData("memberInfo");
}

function logout() {
    showConfirmPopup("로그아웃 하시겠습니까?");
    popupConfirmCallback = function(_ret) {
        if (_ret) {
            removeAllMemberData();
            location.reload();
        }
    };
}

function isLogin() {
    var token = getStorageData("token");
    if (token && token.length > 0) {
        return true;
    } else {
        return false;
    }
}

function isPopupShowing() {
    var isShowing = false;
    $.each($(".layer_wrap"), function(_key, _val) {
        if ($(this).css("display") != "none") {
            isShowing = true;
            $(this).hide();
        }
    });

    return isShowing;
}

function getStorageData(_key) {
    if (!window.localStorage) {
        return;
    }

    if (window.localStorage.getItem(_key) !== null) {
        return JSON.parse(window.localStorage.getItem(_key));
    } else {
        return;
    }
}

function setStorageData(_key, _val) {
    if (!window.localStorage) {
        return;
    }

    window.localStorage.setItem(_key, JSON.stringify(_val));

    return true;
}

function removeStorageData(_key) {
    if (!window.localStorage) {
        return;
    }

    window.localStorage.removeItem(_key);

    return true;
}

function checkValidEmail(_email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(_email);
}

function checkValidId(_id) {
    var re = /^[a-z]+[a-z0-9_-]{5,20}$/g;
    return re.test(_id);
}

function checkValidPw(_pw) {
    var re = /^.*(?=^.{6,20}$)(?=.*\d)(?=.*[a-z]).*$/g;
    return re.test(_pw);
}

// side nav
function openNav() {
    $("#dim").show(0, function() {
        $('#mySidenav').animate({
            left: '0'
        }, 200);
    });
}

function closeNav() {
    $('#mySidenav').animate({
        left: '-80%'
    }, 200, null, function() {
        setTimeout(function() {
            $("#dim").hide();
        }, 100);
    });
}
// end of side nav

function goToHome() {
    location.href = "#/main";
}

function goToShopListPage() {
    location.href = "#/shop_list";
}

function goToNearByShopPage() {
    location.href = "#/shop_list?tab_index=0";
}

function goToRecommendShopPage() {
    location.href = "#/shop_list?tab_index=1";
}

function goToMyNailPage() {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/my_nail";
            }
        };

        return;
    }

    location.href = "#/my_nail";
}

function goToFavoriteShopPage() {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_favorite";
            }
        };

        return;
    }

    location.href = "#/shop_favorite";
}

function goToLocationShopListPage() {
    location.href = "#/location_shop_list";
}

function goToLocationSelectPage() {
    location.href = "#/location_select";
}

function goToLocationSelect2Page() {
    location.href = "#/location_select2";
}

function goToEventListPage(_eventId) {
    location.href = "#/event_list/" + _eventId;
}

function goToShopReviewPage(_shopId, _shopNm) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_write_review/" + _shopId + "/" + _shopNm;
            }
        };

        return;
    }

    location.href = "#/shop_write_review/" + _shopId + "/" + _shopNm;
}

function goToBooking1Page(_shopId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_booking_1/" + _shopId;
            }
        };

        return;
    }

    location.href = "#/shop_booking_1/" + _shopId;
}

function goToBooking2Page(_shopId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_booking_1/" + _shopId;
            }
        };

        return;
    }

    location.href = "#/shop_booking_2/" + _shopId;
}

function goToBooking3Page(_shopId, _orderId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_booking_1/" + _shopId;
            }
        };

        return;
    }

    location.href = "#/shop_booking_3/" + _orderId;
}

function goToPortfolioListPage(_shopId) {
    location.href = "#/shop_portfolio_1/" + _shopId;
}

function goToPortfolioDetailPage(_portId) {
    location.href = "#/shop_portfolio_2/" + _portId;
}

function goToPortfolioHashTagListPage(_shopId, _tagNm) {
    location.href = "#/shop_portfolio_3/" + _shopId + "/" + _tagNm;
}

function goToShopDetailPage(_shopId) {
    location.href = "#/shop_view/" + _shopId;
}

function goToSchedule1Page(_shopId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_schedule_1/" + _shopId;
            }
        };

        return;
    }

    location.href = "#/shop_schedule_1/" + _shopId;
}

function goToSchedule2Page(_shopId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/shop_schedule_2/" + _shopId;
            }
        };

        return;
    }

    location.href = "#/shop_schedule_2/" + _shopId;
}

function goToCouponListPage(_shopId) {
    location.href = "#/coupon/" + _shopId;
}

function goToBookingDetailPage(_orderId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/my_reserved/" + _orderId;
            }
        };

        return;
    }

    location.href = "#/my_reserved/" + _orderId;
}

function goToHistoryDetailPage(_orderId) {
    if (!isLogin()) {
        showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
        popupConfirmCallback = function(_ret) {
            if (_ret) {
                location.href = "#/login?redirectUrl=#/history_detail/" + _orderId;
            }
        };

        return;
    }

    location.href = "#/history_detail/" + _orderId;
}

function updateGcmId(_gcmId) {
    var token = getStorageData("token");
    if (!token || !window.FCMPlugin) {
        return;
    }

    $.ajax({
        url: SERVER_HOST_API + "/gcm.php",
        method: 'POST',
        data: {
            apiType: 'updateGcmId',
            gcmId: _gcmId,
            token: token
        },
        success: function(data) {
            console.log(data);
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function getGcmIdAndUpdate() {
    var token = getStorageData("token");
    if (!token || !window.FCMPlugin) {
        return;
    }

    FCMPlugin.getToken(function(_token) {
        gcmId = _token;

        updateGcmId(gcmId);
    });
}