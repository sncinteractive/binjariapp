angular.module('BinjariApp.controllers', [])
    .factory('BookingData', function() {
        var bookingData = {
            shopId: "",
            pdtId: "",
            pdtNm: "",
            pdtElapseTm: 0,
            managerId: "",
            managerNm: "",
            orderDt: ""
        };

        return {
            getBookingData: function() {
                return bookingData;
            },
            setBookingData: function(_bookingData) {
                bookingData = _bookingData;
            },
            resetBookingData: function() {
                bookingData = {
                    shopId: "",
                    pdtId: "",
                    pdtNm: "",
                    pdtElapseTm: 0,
                    managerId: "",
                    managerNm: "",
                    orderDt: ""
                };
            }
        };
    })
    .controller('MainController', function($scope, $http, $location, $ionicHistory) {
        $scope.goToShopFavoritePage = function() {
            $ionicHistory.clearCache().then(function() {
                goToFavoriteShopPage();
            });
        };

        $scope.goToNearByShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToNearByShopPage();
            });
        };

        $scope.goToRecommendShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToRecommendShopPage();
            });
        };

        $scope.goToMyNailPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToMyNailPage();
            });
        };
    })
    .controller('ShopListController', function($scope, $http, $location, $cordovaGeolocation, $ionicHistory) {
        $scope.page = 1;
        $scope.lat = 0;
        $scope.lng = 0;
        $scope.shop_list = [];
        $scope.event_list = [];

        $scope.Math = window.Math;

        $scope.tab_index = '0';
        if ($location.search().tab_index) {
            $scope.tab_index = $location.search().tab_index;
        }

        $scope.getCurrentLocation = function() {
            var options = { timeout: 10000, enableHighAccuracy: true };

            $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
                $scope.lat = position.coords.latitude;
                $scope.lng = position.coords.longitude;

                $scope.getShopList($scope.tab_index);
            }, function(error) {
                console.log("Could not get location");
                showPopup("현위치 조회에 실패했습니다.");

                // $scope.lat = 37.6350830;
                // $scope.lng = 127.0517380;
                // $scope.getShopList($scope.tab_index);
            });
        };

        $scope.changeShopListTab = function(_tab_index) {
            if (_tab_index) {
                $scope.tab_index = _tab_index;
            }

            $(".shop_list").remove();

            $(".shop_list_tab > span").each(function(key, val) {
                $(this).removeClass("on");
            });

            $(".shop_list_tab > span:eq(" + $scope.tab_index + ")").addClass("on");

            if ($scope.tab_index == '0') {
                $scope.getCurrentLocation($scope.tab_index);
            } else {
                $scope.getShopList($scope.tab_index);
            }
        };

        $scope.getShopList = function() {
            var params = {};
            params.apiType = "shop_list";
            params.lat = $scope.lat; //37.6350830;
            params.lng = $scope.lng; //127.0517380;
            params.range = 200;
            params.page = $scope.page;
            params.shopLocCd = currentLocationTabIndex + ((currentLocationIndex + 1) < 10 ? '0' + (currentLocationIndex + 1) : (currentLocationIndex + 1));

            if ($scope.tab_index == '0') {
                params.shopListType = "distance";
            } else if ($scope.tab_index == '1') {
                params.shopListType = "recommend";
            } else if ($scope.tab_index == '2') {
                params.shopListType = "populate";
            }

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.shop_list = [];

                        $.each(response.data, function(_key, _val) {
                            if (_val.SHOP_IMG1) {
                                _val.SHOP_IMG1 = IMAGE_SERVER_HOST + _val.SHOP_IMG1;
                            }

                            $scope.shop_list.push(_val);
                        });
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.searchByCurrentLocation = function() {
            $scope.tab_index = '0';
            $scope.changeShopListTab();
        };

        $scope.selectRegion = function() {
            var selectedTab = 0;
            $(".shop_list_tab > span").each(function(key, val) {
                if ($(this).hasClass("on")) {
                    selectedTab = key;
                    return;
                }
            });

            $ionicHistory.clearCache().then(function() {
                goToLocationSelectPage();
            });
        };

        $scope.$on('$ionicView.beforeEnter', function() {
            if (isLogin()) {
                var memberInfo = getStorageData("memberInfo");
                $(".hello_master > em").text(memberInfo.USER_NM + "님");

                $("#stateLogout").hide();
                $("#stateLogin").show();
            } else {
                $("#stateLogout").show();
                $("#stateLogin").hide();
            }

            $("#dim").off("click").on("click", function() {
                closeNav();
            });

            $scope.changeShopListTab();

            $(".current_loca").text(LocationData[currentLocationTabIndex][currentLocationIndex]);

            $scope.swiper = new Swiper('#shop_list_event_list', {
                pagination: '.swiper-pagination',
                paginationClickable: true,
                spaceBetween: 30,
            });

            $scope.getEventList();
        });

        $scope.getEventList = function() {
            $("#shop_list_event_list > .swiper-wrapper").empty();

            var params = {};
            params.apiType = "event_list_all";

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.event_list = response.data;

                        $scope.drawEventList();
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.drawEventList = function() {
            $.each($scope.event_list, function(_key, _val) {
                $("#shop_list_event_list > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + _val.EVT_BAN_IMG + "'/></div>");
            });

            $("#shop_list_event_list > .swiper-wrapper > .swiper-slide").off("click").on("click", function() {
                $ionicHistory.clearCache().then(function() {
                    goToEventListPage(1);
                });
            });

            $scope.swiper.init();
        };

        $scope.openNav = function() {
            openNav();
        };

        $scope.goToHome = function() {
            $ionicHistory.clearCache().then(function() {
                goToHome();
            });
        };

        $scope.goToNearByShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToNearByShopPage();
            });
        };

        $scope.goToFavoriteShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToFavoriteShopPage();
            });
        };

        $scope.goToMyNailPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToMyNailPage();
            });
        };

        $scope.goToLocationShopListPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToLocationShopListPage();
            });
        };

        $scope.goToShopDetailPage = function(_shopId) {
            $ionicHistory.clearCache().then(function() {
                goToShopDetailPage(_shopId);
            });
        };
    })
    .controller('LocationSelectController', function($scope, $http, $location, $ionicHistory) {
        $scope.$on('$ionicView.enter', function() {
            $scope.initLocationList();

            $("#location_list_tab > span").off("click").on("click", function() {
                $scope.changeLocationListTab($(this).attr("data-tabindex"));
            });

            $(".loca_list > li").off("click").on("click", function() {
                var selectedTab = 0;
                $("#location_list_tab > span").each(function(key, val) {
                    if ($(this).hasClass("on")) {
                        selectedTab = key;
                        return;
                    }
                });

                var locationIndex = $(this).index('.tab0' + selectedTab + " > .loca_list > li");
                $scope.changeSelectedLocation(selectedTab, locationIndex);

                // goToShopListPage();
                window.history.back();
            });

            $scope.changeLocationListTab(currentLocationTabIndex);
            $scope.changeSelectedLocation(currentLocationTabIndex, currentLocationIndex);
        });

        $scope.changeLocationListTab = function(_tab_index) {
            $("#location_list_tab > span").each(function(key, val) {
                $(this).removeClass("on");
                $('.tab0' + key).hide();
            });

            $("#location_list_tab > span:eq(" + _tab_index + ")").addClass("on");
            $('.tab0' + _tab_index).show();
        };

        $scope.changeSelectedLocation = function(_tab_index, _location_index) {
            $(".loca_list > li").each(function(key, val) {
                $(this).removeClass("on");
                $(this).children(".ico_check_pink").remove();
            });

            $('.tab0' + _tab_index + " > .loca_list > li:eq(" + _location_index + ")").addClass("on");
            $('.tab0' + _tab_index + " > .loca_list > li:eq(" + _location_index + ")").prepend("<i class='sprite ico_check_pink'></i>");

            currentLocationTabIndex = _tab_index;
            currentLocationIndex = _location_index;
        };

        $scope.initLocationList = function() {
            $.each(LocationData, function(key, val) {
                $(".tab0" + key).append("<ul class='loca_list'></ul>");
                $.each(val, function(key2, val2) {
                    $(".tab0" + key + " > .loca_list").append("<li>" + val2 + "</li>");
                });
            });
        };
    })
    .controller('LocationSelect2Controller', function($scope, $http, $state, $cordovaGeolocation, $ionicHistory) {
        $scope.$on('$ionicView.enter', function() {
            $("#btnSetPosition").off("click").on("click", function() {
                history.back();
            });

            mapResize();

            var options = { timeout: 10000, enableHighAccuracy: true };

            $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
                var container = document.getElementById('map'); //지도를 담을 영역의 DOM 레퍼런스
                var options = { //지도를 생성할 때 필요한 기본 옵션
                    center: new daum.maps.LatLng(position.coords.latitude, position.coords.longitude), //지도의 중심좌표.
                    level: 3
                };

                var map = new daum.maps.Map(container, options); //지도 생성 및 객체 리턴

                $scope.marker = new daum.maps.Marker({
                    // 지도 중심좌표에 마커를 생성합니다 
                    position: new daum.maps.LatLng(position.coords.latitude, position.coords.longitude)
                });

                // 지도에 마커를 표시합니다
                $scope.marker.setMap(map);

                $scope.marker.setDraggable(true);

                daum.maps.event.addListener($scope.marker, 'dragend', function() {
                    // 도착 마커의 드래그가 종료될 때 마커 이미지를 원래 이미지로 변경합니다
                    // console.log($scope.marker.getPosition());

                    // $scope.position.lat = marker.getPosition().getLat();
                    // $scope.position.lng = marker.getPosition().getLng();

                    // console.log($scope.position);
                });
            }, function(error) {
                console.log("Could not get location");
                showPopup("현위치 조회에 실패했습니다.");
            });
        });
    })
    .controller('LocationShopListController', function($scope, $http, $state, $cordovaGeolocation, $ionicHistory) {
        $scope.isListOpen = true;
        $scope.page = 1;
        $scope.lat = DEF_LOC_LAT;
        $scope.lng = DEF_LOC_LNG;
        $scope.getNearShopList = function() {
            var options = { timeout: 10000, enableHighAccuracy: true };

            $cordovaGeolocation.getCurrentPosition(options).then(function(position) {
                    $scope.lat = position.coords.latitude;
                    $scope.lng = position.coords.longitude;

                    var container = document.getElementById('map2'); //지도를 담을 영역의 DOM 레퍼런스
                    var options = { //지도를 생성할 때 필요한 기본 옵션
                        center: new daum.maps.LatLng($scope.lat, $scope.lng), //지도의 중심좌표.
                        level: 3
                    };

                    $scope.map = new daum.maps.Map(container, options); //지도 생성 및 객체 리턴

                    var marker = new daum.maps.Marker({
                        // 지도 중심좌표에 마커를 생성합니다 
                        position: new daum.maps.LatLng($scope.lat, $scope.lng)
                    });

                    // 지도에 마커를 표시합니다
                    marker.setMap($scope.map);

                    var params = {};
                    params.apiType = "shop_list";
                    params.lat = $scope.lat; //37.6350830;
                    params.lng = $scope.lng; //127.0517380;
                    params.shopLocCd = currentLocationTabIndex + ((currentLocationIndex + 1) < 10 ? '0' + (currentLocationIndex + 1) : (currentLocationIndex + 1));
                    params.range = 2;
                    params.page = $scope.page;
                    params.shopListType = "distance";

                    $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                        .success(function(response) {
                            console.log("response : " + JSON.stringify(response));

                            $scope.shop_list = [];
                            if (response.result == "100") {
                                $scope.shop_list = response.data;

                                if ($scope.shop_list.length === 0) {
                                    $scope.hideShopList();
                                } else {
                                    $scope.showShopList();
                                }

                                $.each($scope.shop_list, function(_key, _val) {
                                    var marker = new daum.maps.Marker({
                                        position: new daum.maps.LatLng(_val.SHOP_LAT, _val.SHOP_LNG)
                                    });

                                    marker.setMap($scope.map);
                                });
                            } else {
                                showPopup(response.message);
                                return false;
                            }
                        })
                        .error(function(error) {
                            console.log("error", error);
                        });
                },
                function(error) {
                    console.log("Could not get location");
                    showPopup("현위치 조회에 실패했습니다.");
                });
        };

        $scope.mapResize = function() {
            var docH = $(document).height();

            $('.map_area').css({
                height: docH - ($('.sub_title').height())
            });
        };

        $scope.$on('$ionicView.enter', function(event, data) {
            $(".current_location").off("click").on("click", function() {
                $ionicHistory.clearCache().then(function() {
                    goToLocationSelectPage();
                });
            });

            $(".current_location").text(LocationData[currentLocationTabIndex][currentLocationIndex]);

            $scope.mapResize();

            $scope.getNearShopList();

            $scope.hideShopList();
        });

        $scope.moveCurrentLocation = function() {
            if ($scope.map) {
                $scope.map.setCenter(new daum.maps.LatLng($scope.lat, $scope.lng));
            }
        };

        $scope.goToShopDetailPage = function(_shopId) {
            $ionicHistory.clearCache().then(function() {
                goToShopDetailPage(_shopId);
            });
        };

        $scope.hideShopList = function() {
            $scope.isListOpen = false;
            $(".ft_fix_area3").css({ height: "0px" });
            $(".btnListClose").css({ bottom: "0px" });
        };

        $scope.showShopList = function() {
            $scope.isListOpen = true;
            if ($scope.shop_list.length < 3) {
                $(".ft_fix_area3").css({ height: (73 * $scope.shop_list.length) + "px" });
                $(".btnListClose").css({ bottom: (73 * $scope.shop_list.length - 1) + "px" });
            } else {
                $(".ft_fix_area3").css({ height: "220px" });
                $(".btnListClose").css({ bottom: "219px" });
            }
        };

        $scope.toggleShopList = function() {
            if ($scope.isListOpen) {
                $scope.hideShopList();
            } else {
                $scope.showShopList();
            }
        };
    })
    .controller('ShopViewController', function($scope, $http, $stateParams, $location, $cordovaGeolocation, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.shopDetail = {};
        $scope.shopPdtList = [];
        $scope.shopPortfolioList = [];
        $scope.shopReviewList = [];
        $scope.shopEventList = [];
        $scope.shopCouponList = [];

        var swiper = null;
        $scope.Math = window.Math;

        $scope.changeShopDetailTab = function(_tab_index) {
            $("#shop_detail_tab > span").each(function(key, val) {
                $(this).removeClass("on");
                // $('.tab0' + key).hide();
            });

            $("#shop_detail_tab2 > span").each(function(key, val) {
                $(this).removeClass("on");
                // $('.tab0' + key).hide();
            });

            $("#shop_detail_tab > span:eq(" + _tab_index + ")").addClass("on");
            $("#shop_detail_tab2 > span:eq(" + _tab_index + ")").addClass("on");
            // $('.tab0' + _tab_index).show();
            if (swiper !== null) {
                swiper.slideTo(_tab_index);
            }
        };

        $scope.goToReview = function() {
            $ionicHistory.clearCache().then(function() {
                goToShopReviewPage($scope.shopId, $scope.shopDetail.SHOP_NM);
            });
        };

        $scope.showMoreReview = function(_idx) {
            $(".review_list > li:eq(" + _idx + ") > p.guest_review").removeClass("multi_ellipsis");
            $(".review_list > li:eq(" + _idx + ") > p.master_reply").show();
            $(".review_list > li:eq(" + _idx + ") > .btn_more").hide();
        };

        $scope.goToReservation = function() {
            $ionicHistory.clearCache().then(function() {
                goToBooking1Page($scope.shopId);
            });
        };

        $scope.goToPortfolioListPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToPortfolioListPage($scope.shopId);
            });
        };

        $scope.addFavoriteShop = function() {
            if (!isLogin()) {
                showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        location.href = "#/login?redirectUrl=#/shop_view/" + $scope.shopId;
                    }
                };

                return;
            }

            var params = {};
            params.apiType = "add_favorite_shop";
            params.token = getStorageData("token");
            params.shopId = $scope.shopId;

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        showPopup("찜 목록에 등록 되었습니다.");
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.$on('$ionicView.loaded', function() {
            var topBarOffset = $("#topBar2").offset();

            $(".shopDetailScrollView").off("scroll").on("scroll", function() {
                var docScrollY = $(".shopDetailScrollView").scrollTop();

                console.log("docScrollY : " + docScrollY + ", topBarOffset.top : " + topBarOffset.top);

                if ((docScrollY - 46) >= topBarOffset.top) {
                    $("#topBar").show();
                    // $("#topBar2").hide();
                    $("#topBar .tab span").css("border-top", "0");
                } else {
                    $("#topBar").hide();
                    // $("#topBar2").show();
                }
            });
        });

        var params = {};
        params.apiType = "shop_detail";
        params.shopId = $scope.shopId;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                if (response.result == "100") {
                    if (response.data) {
                        $scope.shopDetail = response.data.shopDetail;
                        $scope.shopPdtList = response.data.shopPdtList;
                        $scope.shopPortfolioList = response.data.shopPortfolioList;
                        $scope.shopReviewList = response.data.shopReviewList;
                        $scope.shopEventList = response.data.shopEventList;
                        $scope.shopCouponList = response.data.shopCouponList;

                        $scope.shopDetail.SHOP_ETC = $scope.shopDetail.SHOP_ETC.replace(/etc1_/, '').replace(/etc2_/, '');

                        $.each($scope.shopPortfolioList, function(_key, _val) {
                            _val.PORT_IMG1 = IMAGE_SERVER_HOST + _val.PORT_IMG1;
                        });

                        if ($scope.shopPortfolioList.length <= 0) {
                            $scope.shopPortfolioList.push({ PORT_IMG1: "" });
                        }

                        $scope.shopOpTime = [];
                        if ($scope.shopDetail.SHOP_OP_TIME) {
                            var optimeArray = $scope.shopDetail.SHOP_OP_TIME.split("|");

                            for (var i = 0; i < optimeArray.length; i++) {
                                if (i > 0 && optimeArray[i] && optimeArray[i].length > 0) {
                                    var tmp = optimeArray[i].split("-");
                                    if (tmp.length > 2 && tmp[2] == "N") {
                                        $scope.shopOpTime.push(tmp[0] + " " + tmp[1]);
                                    }
                                } else if (i === 0) {
                                    $scope.shopOpTime.push(optimeArray[i].replace("-", " "));
                                }
                            }
                        }

                        $scope.shopOpTime.push($scope.shopDetail.SHOP_OP_TIME2);

                        $scope.showMapMarker($scope.shopDetail.SHOP_LAT, $scope.shopDetail.SHOP_LNG);

                        setTimeout(function() {
                            $scope.swiper = new Swiper('#shop_edit_detail_shopimage', {
                                pagination: '.swiper-pagination',
                                paginationClickable: true,
                                spaceBetween: 30,
                            });

                            swiper = new Swiper('#fixNextTag > .swiper-container', {
                                spaceBetween: 30,
                                autoHeight: true,
                                onSlideChangeEnd: function(_data) {
                                    $scope.changeShopDetailTab(_data.realIndex);
                                }
                            });

                            if ($scope.shopDetail.SHOP_IMG1) {
                                $("#shop_edit_detail_shopimage > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG1 + "' /></div>");
                            }

                            if ($scope.shopDetail.SHOP_IMG2) {
                                $("#shop_edit_detail_shopimage > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG2 + "' /></div>");
                            }

                            if ($scope.shopDetail.SHOP_IMG3) {
                                $("#shop_edit_detail_shopimage > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG3 + "' /></div>");
                            }

                            $scope.swiper.init();
                        }, 300);

                        $scope.changeShopDetailTab(0);
                    }
                } else {
                    showPopup(response.message);
                    return false;
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.showMapMarker = function(_lat, _lng) {
            var container = document.getElementById('shop_detail_map'); //지도를 담을 영역의 DOM 레퍼런스
            var options = { //지도를 생성할 때 필요한 기본 옵션
                center: new daum.maps.LatLng(_lat, _lng), //지도의 중심좌표.
                level: 3, //지도의 레벨(확대, 축소 정도)
                draggable: true
            };

            var map = new daum.maps.Map(container, options); //지도 생성 및 객체 리턴

            var marker = new daum.maps.Marker({
                // 지도 중심좌표에 마커를 생성합니다 
                position: new daum.maps.LatLng(_lat, _lng)
            });

            // 지도에 마커를 표시합니다
            marker.setMap(map);
        };
    })
    .controller('ShopPortfolio1Controller', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.shopDetail = {};
        $scope.shopPortfolioList = [];

        $scope.showPortfolioDetail = function(_portId) {
            $ionicHistory.clearCache().then(function() {
                goToPortfolioDetailPage(_portId);
            });
        };

        var params = {};
        params.apiType = "shop_portfolio";
        params.shopId = $scope.shopId;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.shopPortfolioList = response.data.shopPortfolioList;
                        $scope.shopDetail = response.data.shopDetail;

                        $.each($scope.shopPortfolioList, function(_key, _val) {
                            _val.PORT_IMG1 = IMAGE_SERVER_HOST + _val.PORT_IMG1;
                        });

                        setTimeout(function() {
                            $scope.swiper = new Swiper('#shop_portfolio_shopimage', {
                                pagination: '.swiper-pagination',
                                paginationClickable: true,
                                spaceBetween: 30,
                            });

                            if ($scope.shopDetail.SHOP_IMG1) {
                                $("#shop_portfolio_shopimage > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG1 + "' /></div>");
                            }

                            if ($scope.shopDetail.SHOP_IMG2) {
                                $("#shop_portfolio_shopimage > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG2 + "' /></div>");
                            }

                            if ($scope.shopDetail.SHOP_IMG3) {
                                $("#shop_portfolio_shopimage > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG3 + "' /></div>");
                            }

                            $scope.swiper.init();
                        }, 300);
                    }
                } else {
                    showPopup(response.message);
                    return false;
                }
            })
            .error(function(error) {
                console.log("error", error);
            });
    })
    .controller('ShopPortfolio2Controller', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.portId = $stateParams.portId;
        $scope.shopDetail = {};
        $scope.portfolioDetail = {};
        $scope.portfolioHashTagList = [];

        $scope.addFavoriteShop = function() {
            if (!isLogin()) {
                showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        location.href = "#/login?redirectUrl=#/shop_portfolio_2/" + $scope.portId;
                    }
                };

                return;
            }

            var params = {};
            params.apiType = "add_favorite_shop";
            params.token = getStorageData("token");
            params.shopId = $scope.shopDetail.SHOP_ID;

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        showPopup("찜 목록에 등록 되었습니다.");
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.showPortfolioByHashTag = function(_tagNm) {
            $ionicHistory.clearCache().then(function() {
                _tagNm = _tagNm.replace('#', '');
                goToPortfolioHashTagListPage($scope.shopDetail.SHOP_ID, _tagNm);
            });
        };

        $scope.goToShopDetailPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToShopDetailPage($scope.shopDetail.SHOP_ID);
            });
        };

        var params = {};
        params.apiType = "portfolio_detail";
        params.portId = $scope.portId;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.portfolioDetail = response.data.portfolioDetail;
                        $scope.portfolioHashTagList = response.data.portfolioHashList;
                        $scope.shopDetail = response.data.shopDetail;

                        if ($scope.portfolioDetail.PORT_IMG1) {
                            $scope.portfolioDetail.PORT_IMG1 = IMAGE_SERVER_HOST + $scope.portfolioDetail.PORT_IMG1;
                        }

                        if ($scope.shopDetail.SHOP_IMG1) {
                            $scope.shopDetail.SHOP_IMG1 = IMAGE_SERVER_HOST + $scope.shopDetail.SHOP_IMG1;
                        }
                    }
                } else {
                    showPopup(response.message);
                    return false;
                }
            })
            .error(function(error) {
                console.log("error", error);
            });
    })
    .controller('ShopPortfolio3Controller', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.tagNm = '#' + $stateParams.tagNm;
        $scope.shopDetail = {};
        $scope.portfolioList = [];

        $scope.goToPortfolioDetail = function(_portId) {
            $ionicHistory.clearCache().then(function() {
                goToPortfolioDetailPage(_portId);
            });
        };

        $scope.goToScrollTop = function() {
            $('ion-content').animate({
                scrollTop: 0
            }, 'fast');
        };

        var params = {};
        params.apiType = "portfolio_list_by_hashtag";
        params.shopId = $scope.shopId;
        params.tagNm = $scope.tagNm;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.portfolioList = response.data.portfolioList;
                        $scope.shopDetail = response.data.shopDetail;

                        $.each($scope.portfolioList, function(_key, _val) {
                            _val.PORT_IMG1 = IMAGE_SERVER_HOST + _val.PORT_IMG1;
                        });
                    }
                } else {
                    showPopup(response.message);
                    return false;
                }
            })
            .error(function(error) {
                console.log("error", error);
            });
    })
    .controller('ShopBooking1Controller', function($scope, $http, $stateParams, BookingData, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.availableCouponCount = 0;
        $scope.couponList = [];
        $scope.bookingData = {};
        $scope.bookingDateStr = "";
        $scope.selectedPaymentMethod = 0;

        var params = {};
        params.apiType = "booking_info";
        params.token = getStorageData("token");
        params.shopId = $scope.shopId;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.couponList = response.data.couponList;

                        $scope.setCouponList();

                        $scope.selectPaymentRadio(0);
                    }
                } else if (response.result == "300") {
                    removeAllMemberData();

                    showPopup(response.message);
                    popupCallback = function() {
                        location.replace('#/login');
                    };
                    return false;
                } else {
                    showPopup(response.message);
                    return false;
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.setCouponList = function() {
            $("#usableCouponList").append("<option value='0'>사용 가능 쿠폰 " + $scope.couponList.length + "장</option>");

            $.each($scope.couponList, function(_key, _val) {
                $("#usableCouponList").append("<option value='" + _val.SHOP_COUP_ID + "'>" + _val.COUP_NM + "</option>");
            });

            $("#usableCouponList").append();
        };

        $scope.selectPaymentRadio = function(_selectIdx) {
            $("#paymentSelectRadio > .frList > .designRadio > .radio").each(function(key, val) {
                $(this).removeClass("checked");
            });

            $("#paymentSelectRadio > .frList:eq(" + _selectIdx + ") > .designRadio > .radio").addClass("checked");

            $scope.selectedPaymentMethod = _selectIdx;
        };

        $scope.goToSchedule2Page = function() {
            $ionicHistory.clearCache().then(function() {
                goToSchedule2Page($scope.shopId);
            });
        };

        $scope.goToSchedule1Page = function() {
            $scope.bookingData = BookingData.getBookingData();

            if (!$scope.bookingData.pdtId) {
                showPopup("상품을 먼저 선택해 주세요.");
            } else {
                $ionicHistory.clearCache().then(function() {
                    goToSchedule1Page($scope.shopId);
                });
            }
        };

        $scope.goToCouponListPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToCouponListPage($scope.shopId);
            });
        };

        $scope.payment = function() {
            $scope.bookingData = BookingData.getBookingData();

            if (!$scope.bookingData.pdtId) {
                showPopup("예약 상품을 선택해 주세요.");
                return;
            }

            if (!$scope.bookingData.orderDt) {
                showPopup("예약 시간을 선택해 주세요.");
                return;
            }

            if ($scope.selectedPaymentMethod == 1) {
                showPopup("현장 결제만 가능합니다.");
                return;
            }

            var params = {};
            params.apiType = "booking";
            params.token = getStorageData("token");
            params.shopId = $scope.shopId;
            params.pdtId = $scope.bookingData.pdtId;
            params.managerId = $scope.bookingData.managerId;
            params.orderDt = $scope.bookingData.orderDt;
            params.shopCoupId = $("#usableCouponList").val();
            params.orderGubunCd = "000";

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.bookingData = {};
                        $scope.bookingDateStr = "";
                        BookingData.resetBookingData();

                        $ionicHistory.clearCache().then(function() {
                            goToBooking3Page($scope.shopId, response.data.orderId);
                        });
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.$on('$ionicView.enter', function() {
            $scope.bookingData = BookingData.getBookingData();
            $scope.bookingData.shopId = $stateParams.shopId;

            BookingData.setBookingData($scope.bookingData);

            if ($scope.bookingData.orderDt) {
                var startTime = new Date(($scope.bookingData.orderDt.substr(0, 4) * 1), (($scope.bookingData.orderDt.substr(4, 2) * 1) - 1), ($scope.bookingData.orderDt.substr(6, 2) * 1), ($scope.bookingData.orderDt.substr(8, 2) * 1), ($scope.bookingData.orderDt.substr(10, 2) * 1));
                var endTime = new Date(startTime.getTime() + ($scope.bookingData.pdtElapseTm * 60 * 1000));
                var startTimeStr = startTime.getFullYear();
                startTimeStr += "." + ((startTime.getMonth() + 1) < 10 ? '0' + (startTime.getMonth() + 1) : (startTime.getMonth() + 1));
                startTimeStr += "." + (startTime.getDate() < 10 ? '0' + startTime.getDate() : startTime.getDate());
                startTimeStr += " " + (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours());
                startTimeStr += ":" + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());
                var endTimeStr = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours());
                endTimeStr += ":" + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());

                $scope.bookingDateStr = startTimeStr + " ~ " + endTimeStr;
            }
        });
    })
    .controller('ShopCouponListController', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.couponList = [];
        $scope.shopDetail = {};

        $scope.$on('$ionicView.enter', function() {
            $scope.getCouponList();
        });

        $scope.getCouponList = function() {
            var params = {};
            params.token = getStorageData("token");
            params.shopId = $scope.shopId;
            params.apiType = "shop_coupon_list";

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            $scope.couponList = response.data.couponList;
                            $scope.shopDetail = response.data.shopDetail;
                        }
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.couponDownload = function(_shopCouponId) {
            showConfirmPopup("쿠폰을 다운로드 하시겠습니까?");
            popupConfirmCallback = function(_ret) {
                if (_ret) {
                    var params = {};
                    params.token = getStorageData("token");
                    params.shopId = $scope.shopId;
                    params.shopCouponId = _shopCouponId;
                    params.apiType = "recv_coupon";

                    $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                        .success(function(response) {
                            console.log("response : " + JSON.stringify(response));

                            if (response.result == "100") {
                                showPopup("쿠폰이 발급되었습니다.");
                                $scope.getCouponList();
                            } else if (response.result == "300") {
                                removeAllMemberData();

                                showPopup(response.message);
                                popupCallback = function() {
                                    location.replace('#/login');
                                };
                                return false;
                            } else {
                                showPopup(response.message);
                                return false;
                            }
                        })
                        .error(function(error) {
                            console.log("error", error);
                        });
                }
            };
        };
    })
    .controller('ShopSchedule1Controller', function($scope, $http, $stateParams, BookingData, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.shopManagerList = [];
        $scope.shopBookingList = [];
        $scope.todayMonth = "";
        $scope.availableBookingDate = [];
        $scope.selectedManager = "";

        $scope.fulldate = "";
        $scope.selectedBookingElement = null;

        $scope.$on('$ionicView.enter', function() {
            var params = {};
            params.apiType = "shop_booking_list";
            params.shopId = $scope.shopId;
            params.token = getStorageData("token");

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            $scope.shopManagerList = response.data.shopManagerList;
                            $scope.shopBookingList = response.data.shopBookingList;
                            $scope.availableBookingDate = response.data.availableBookingDate;
                        }
                    } else {
                        showPopup(response.message);
                    }

                    $scope.galleryThumbs();

                    $scope.drawCalendar();

                    var galleryTop = new Swiper('#schedule1Swiper', {
                        spaceBetween: 10,
                        onSlideChangeEnd: function(_data) {
                            $scope.todayMonth = $scope.availableBookingDate[_data.realIndex].month;
                            $(".year_month").text($scope.todayMonth);
                        }
                    });

                    var galleryThumbs = new Swiper('.gallery-thumbs', {
                        spaceBetween: 5,
                        // centeredSlides: true,
                        centeredSlides: true,
                        slidesPerView: 'auto',
                        touchRatio: 0.2,
                        slideToClickedSlide: true
                    });

                    galleryTop.params.control = galleryThumbs;
                    galleryThumbs.params.control = galleryTop;
                })
                .error(function(error) {
                    console.log("error", error);
                });
        });

        $scope.galleryThumbs = function() {
            var html = "";

            html += "<div class='swiper-wrapper'>";
            for (var i = 0; i < $scope.availableBookingDate.length; i++) {
                html += "<div class='swiper-slide'>" + $scope.availableBookingDate[i].date + "</div>";
            }
            html += "</div>";

            $scope.todayMonth = $scope.availableBookingDate[0].month;

            $(".gallery-thumbs").html(html);
            $(".year_month").text($scope.todayMonth);
        };

        $scope.drawCalendar = function() {
            var startTime = 9;
            var endTime = 20;

            var html = "";

            $.each($scope.availableBookingDate, function(key1, val1) {
                var bookingMonthDate = $scope.availableBookingDate[key1].month.replace(".", "") + $scope.availableBookingDate[key1].date;

                html += "<div class='swiper-slide' id='swiper-slide-" + bookingMonthDate + "'>";
                html += "   <div class='swiper_inner_pd'>";
                html += "       <div class='schd_lst_wrap'>";

                if ($scope.shopBookingList[bookingMonthDate]) {
                    $.each($scope.shopBookingList[bookingMonthDate], function(key, val) {
                        var startTime = new Date(val.ORDER_DT.substr(0, 4) * 1, val.ORDER_DT.substr(4, 2) * 1, val.ORDER_DT.substr(6, 2) * 1, val.ORDER_DT.substr(8, 2) * 1, val.ORDER_DT.substr(10, 2) * 1, val.ORDER_DT.substr(12, 2) * 1);
                        var startTimeStr = (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) + ":" + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());

                        var endTime = new Date(startTime.getTime() + (val.ELAPSE_TM * 60 * 1000));
                        var endTimeStr = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours()) + ":" + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());

                        html += $scope.getBookingHtml(startTime.getHours(), val.ORDER_DT.substr(10, 2), val.ELAPSE_TM, startTimeStr, endTimeStr, val.USER_NM, val.PDT_NM);
                    });
                }

                html += "<ul class='schd_lst_bg'>";

                for (var i = 0; i <= 23; i++) {
                    var hours = i < 10 ? '0' + i : i;
                    var amPm = i < 12 ? "AM" : "PM";

                    html += "<li>";
                    html += "   <span>" + hours + ":00 " + amPm + "</span>";
                    html += "   <div class='half' data-time='" + bookingMonthDate + hours + "00'>";
                    html += "       <i> </i>";
                    html += "   </div>";
                    html += "   <div class='half' data-time='" + bookingMonthDate + hours + "30'>";
                    html += "       <i> </i>";
                    html += "   </div>";
                    html += "</li>";
                }

                html += "           </ul>";
                html += "       </div>";
                html += "   </div>";
                html += "</div>";
            });

            $("#swiperContent").html(html);

            $(".schd_lst_bg > li > .half").off("click").on("click", function() {
                $scope.fulldate = $(this).attr("data-time");
                var bookingData = BookingData.getBookingData();
                var bookingMonthDate = $scope.fulldate.substr(0, 8);
                var hours = $scope.fulldate.substr(8, 2) * 1;
                var minutes = $scope.fulldate.substr(10, 2);
                var startTime = new Date($scope.fulldate.substr(0, 4) * 1, $scope.fulldate.substr(4, 2) * 1, $scope.fulldate.substr(6, 2) * 1, $scope.fulldate.substr(8, 2) * 1, $scope.fulldate.substr(10, 2) * 1);
                var endTime = new Date(startTime.getTime() + (bookingData.pdtElapseTm * 60 * 1000));
                var managerName = $("#shopManagerSelect").text();

                var startTimeStr = (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) + ":" + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());
                var endTimeStr = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours()) + ":" + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());

                var popupHtml = "";
                popupHtml += "<dl class='booking_info' style='margin-top: -24px;'>";
                popupHtml += "<dt>담당자:</dt>";
                popupHtml += "<dd>" + managerName + "</dd>";
                popupHtml += "<dt>예약시간:</dt>";
                popupHtml += "<dd>" + startTimeStr + " ~ " + endTimeStr + "</dd>";
                popupHtml += "</dl>";
                popupHtml += "선택 하시겠습니까?";

                showConfirmPopup(popupHtml);
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        if ($scope.selectedBookingElement !== null) {
                            $scope.selectedBookingElement.remove();
                            $scope.selectedBookingElement = null;
                        }

                        var html = $scope.getBookingHtml(hours, minutes, bookingData.pdtElapseTm, startTimeStr, endTimeStr, managerName, bookingData.pdtNm);
                        $scope.selectedBookingElement = $(html);
                        $('#swiper-slide-' + bookingMonthDate + ' > .swiper_inner_pd > .schd_lst_wrap').prepend($scope.selectedBookingElement);
                    }
                };
            });
        };

        $scope.getBookingHtml = function(_startHours, _startMinutes, _elapseTime, _startTimeStr, _endTimeStr, _managerName, _pdtName) {
            var html = "";
            if (_startHours < 12) {
                html += "<div class='scheduled am" + _startHours + "_" + _startMinutes + " minute" + _elapseTime + "'>";
            } else {
                if (_startHours > 12) {
                    _startHours = _startHours - 12;
                }
                html += "<div class='scheduled pm" + _startHours + "_" + _startMinutes + " minute" + _elapseTime + "'>";
            }

            if (_elapseTime == 30) {
                html += "<em style='float: left; margin-right: 5px;'>" + _startTimeStr + " ~ " + _endTimeStr + "</em>" + _managerName + " / " + _pdtName;
            } else {
                html += "<em>" + _startTimeStr + " ~ " + _endTimeStr + "</em>" + _managerName + " / " + _pdtName;
            }

            html += "</div>";
            return html;
        };

        $scope.saveSchedule = function() {
            var bookingData = BookingData.getBookingData();

            bookingData.orderDt = $scope.fulldate;
            bookingData.managerId = $("#shopManagerSelect").val();
            bookingData.managerNm = $("#shopManagerSelect").text();
            BookingData.setBookingData(bookingData);

            window.history.back();
        };
    })
    .controller('ShopSchedule2Controller', function($scope, $http, $stateParams, BookingData, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.shopPdtList = [];

        var params = {};
        params.apiType = "shop_pdt_list";
        params.shopId = $scope.shopId;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.shopPdtList = response.data;
                    }
                } else {
                    showPopup(response.message);
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.selectedProduct = function(_pdtId, _pdtNm, _pdtElapseTm) {
            showConfirmPopup(_pdtNm + " 상품을<br/>선택하시겠습니까?");
            popupConfirmCallback = function(_ret) {
                if (_ret) {
                    var bookingData = BookingData.getBookingData();
                    bookingData.pdtId = _pdtId;
                    bookingData.pdtNm = _pdtNm;
                    bookingData.pdtElapseTm = _pdtElapseTm;

                    BookingData.setBookingData(bookingData);

                    window.history.back();
                }
            };
        };

        $scope.$on('$ionicView.enter', function() {
            /* 선택된값의 value는 _value 에 있음 */
            var _designRadio = $('.designRadio');
            var _iLabel = $('.iLabel');

            $(_iLabel).click(function() {
                var _thisRadio = $(this).parent().find('> .designRadio');
                var _value = $(this).parent().find('>input').val();
                $(_designRadio).children().removeClass('checked');
                $(_thisRadio).children().addClass('checked');
            });

            $(_designRadio).click(function() {
                var _value = $(this).parent().find('>input').val();
                $(_designRadio).children().removeClass('checked');
                $(this).children().addClass('checked');
            });
        });
    })
    .controller('ShopBooking2Controller', function($scope, $http, $ionicHistory) {
        $scope.$on('$ionicView.enter', function() {
            /* 선택된값의 value는 _value 에 있음 */
            var _designRadio = $('.designRadio');
            var _iLabel = $('.iLabel');

            $(_iLabel).click(function() {
                var _thisRadio = $(this).parent().find('> .designRadio');
                var _value = $(this).parent().find('>input').val();
                $(_designRadio).children().removeClass('checked');
                $(_thisRadio).children().addClass('checked');
            });

            $(_designRadio).click(function() {
                var _value = $(this).parent().find('>input').val();
                $(_designRadio).children().removeClass('checked');
                $(this).children().addClass('checked');
            });

            $("#btnPaymentProc").off("click").on("click", function() {
                location.href = "#/shop_booking_3";
            });
        });
    })
    .controller('ShopBooking3Controller', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.orderId = $stateParams.orderId;
        $scope.orderDetail = {};

        var params = {};
        params.apiType = "booking_detail";
        params.orderId = $scope.orderId;
        params.token = getStorageData("token");

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.orderDetail = response.data;
                        $scope.orderDetail.bookingMonthDate = $scope.orderDetail.ORDER_DT.substr(0, 4) + "년 " + $scope.orderDetail.ORDER_DT.substr(4, 2) + "월 " + $scope.orderDetail.ORDER_DT.substr(6, 2) + "일";

                        var startTime = new Date(($scope.orderDetail.ORDER_DT.substr(0, 4) * 1), ($scope.orderDetail.ORDER_DT.substr(4, 2) * 1) - 1, ($scope.orderDetail.ORDER_DT.substr(6, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(8, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(10, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(12, 2) * 1));
                        var endTime = new Date(startTime.getTime() + ($scope.orderDetail.ELAPSE_TM * 60 * 1000));

                        $scope.orderDetail.startTimeStr = (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) + ":" + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());
                        $scope.orderDetail.endTimeStr = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours()) + ":" + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());
                    }
                } else {
                    showPopup(response.message);
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.$on('$ionicView.enter', function() {

        });

        $scope.addFavoriteShop = function() {
            if (!isLogin()) {
                showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        location.href = "#/login?redirectUrl=#/shop_view/" + $scope.shopId;
                    }
                };

                return;
            }

            var params = {};
            params.apiType = "add_favorite_shop";
            params.token = getStorageData("token");
            params.shopId = $scope.orderDetail.SHOP_ID;

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        showPopup("찜 목록에 등록 되었습니다.");
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.goToShopList = function() {
            $ionicHistory.clearCache().then(function() {
                goToShopListPage();
            });
        };
    })
    .controller('MyNailController', function($scope, $http, $ionicHistory) {
        $scope.currentTab = 0;
        $scope.bookingList = [];
        $scope.historyList = [];

        $scope.currentDate = new Date();

        $scope.datePickerCallback = function(val) {
            if (!val) {
                console.log('Date not selected');
            } else {
                console.log('Selected date is : ', val);
                $scope.currentDate = val;

                $scope.changeBookingTab($scope.currentTab);
            }
        };

        $scope.$on('$ionicView.enter', function() {
            if (isLogin()) {
                var memberInfo = getStorageData("memberInfo");
                $(".hello_master > em").text(memberInfo.USER_NM + "님");

                $("#stateLogout").hide();
                $("#stateLogin").show();
            } else {
                $("#stateLogout").show();
                $("#stateLogin").hide();
            }

            $("#dim").off("click").on("click", function() {
                closeNav();
            });

            $scope.changeBookingTab($scope.currentTab);
        });

        $scope.openNav = function() {
            openNav();
        };

        $scope.changeBookingTab = function(_idx) {
            $scope.currentTab = _idx;

            $("#myBookingListTab > span").each(function(key, val) {
                $(this).removeClass("on");
                $('.myBookingListTab0' + key).hide();
                $('.myBookingListTab0' + key + "_wrapper").hide();
            });

            $("#myBookingListTab > span:eq(" + _idx + ")").addClass("on");
            $('.myBookingListTab0' + _idx).show();
            $('.myBookingListTab0' + _idx + "_wrapper").show();

            var params = {};
            params.token = getStorageData("token");

            if ($scope.currentTab === 0) {
                params.apiType = "my_booking_list";
            } else {
                var startDate = new Date();
                startDate.setYear($scope.currentDate.getFullYear());
                startDate.setMonth($scope.currentDate.getMonth());
                startDate.setDate($scope.currentDate.getDate());

                var endDate = new Date(startDate.getTime());
                endDate.setDate(startDate.getDate() + 1);

                params.apiType = "my_history_list";
                params.startDate = startDate.getFullYear();
                params.startDate += (startDate.getMonth() + 1) < 10 ? '0' + (startDate.getMonth() + 1) : (startDate.getMonth() + 1);
                params.startDate += startDate.getDate() < 10 ? '0' + startDate.getDate() : startDate.getDate();
                params.startDate += "000000";

                params.endDate = endDate.getFullYear();
                params.endDate += (endDate.getMonth() + 1) < 10 ? '0' + (endDate.getMonth() + 1) : (endDate.getMonth() + 1);
                params.endDate += endDate.getDate() < 10 ? '0' + endDate.getDate() : endDate.getDate();
                params.endDate += "000000";
            }

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            if ($scope.currentTab === 0) {
                                $scope.bookingList = response.data;

                                $.each($scope.bookingList, function(_key, _val) {
                                    if (_val.SHOP_IMG1) {
                                        _val.SHOP_IMG1 = IMAGE_SERVER_HOST + _val.SHOP_IMG1;
                                    }
                                });
                            } else {
                                $scope.historyList = response.data;

                                $.each($scope.historyList, function(_key, _val) {
                                    if (_val.SHOP_IMG1) {
                                        _val.SHOP_IMG1 = IMAGE_SERVER_HOST + _val.SHOP_IMG1;
                                    }
                                });
                            }
                        }
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.goToBookingDetail = function(_orderId) {
            $ionicHistory.clearCache().then(function() {
                goToBookingDetailPage(_orderId);
            });
        };

        $scope.goToHistoryDetail = function(_orderId) {
            $ionicHistory.clearCache().then(function() {
                goToHistoryDetailPage(_orderId);
            });
        };

        $scope.goToHome = function() {
            $ionicHistory.clearCache().then(function() {
                goToHome();
            });
        };

        $scope.goToNearByShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToNearByShopPage();
            });
        };

        $scope.goToFavoriteShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToFavoriteShopPage();
            });
        };

        $scope.goToMyNailPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToMyNailPage();
            });
        };

        $scope.goToLocationShopListPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToLocationShopListPage();
            });
        };
    })
    .controller('MyBookingDetailController', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.orderId = $stateParams.orderId;
        $scope.orderDetail = {};

        var params = {};
        params.apiType = "booking_detail";
        params.orderId = $scope.orderId;
        params.token = getStorageData("token");

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.orderDetail = response.data;
                        $scope.orderDetail.bookingMonthDate = $scope.orderDetail.ORDER_DT.substr(0, 4) + "." + $scope.orderDetail.ORDER_DT.substr(4, 2) + "." + $scope.orderDetail.ORDER_DT.substr(6, 2);

                        var startTime = new Date(($scope.orderDetail.ORDER_DT.substr(0, 4) * 1), ($scope.orderDetail.ORDER_DT.substr(4, 2) * 1) - 1, ($scope.orderDetail.ORDER_DT.substr(6, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(8, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(10, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(12, 2) * 1));
                        var endTime = new Date(startTime.getTime() + ($scope.orderDetail.ELAPSE_TM * 60 * 1000));

                        $scope.orderDetail.startTimeStr = (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) + ":" + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());
                        $scope.orderDetail.endTimeStr = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours()) + ":" + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());
                    }
                } else {
                    showPopup(response.message);
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.$on('$ionicView.enter', function() {

        });

        $scope.goToReview = function() {
            $ionicHistory.clearCache().then(function() {
                goToShopReviewPage($scope.orderDetail.SHOP_ID, $scope.orderDetail.SHOP_NM);
            });
        };

        $scope.cancelBooking = function() {
            if ($scope.orderDetail.ORDER_TP_CD == "000" || $scope.orderDetail.ORDER_TP_CD == "002") {
                showConfirmPopup("예약을 취소하시겠습니까?");
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        var params = {};
                        params.apiType = "cancel_shop_booking_customer";
                        params.token = getStorageData("token");
                        params.bookingId = $scope.orderId;
                        params.shopId = $scope.orderDetail.SHOP_ID;
                        params.cancelFrom = "customer";

                        console.log("params : " + JSON.stringify(params));

                        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                            .success(function(response) {
                                console.log("response : " + JSON.stringify(response));

                                if (response.result == "100") {
                                    showPopup("예약이 취소 되었습니다.");

                                    popupCallback = function() {
                                        $ionicHistory.clearCache().then(function() {
                                            goToMyNailPage();
                                        });
                                    };
                                } else if (response.result == "300") {
                                    removeAllMemberData();

                                    showPopup(response.message);
                                    popupCallback = function() {
                                        location.replace('#/login');
                                    };
                                    return false;
                                } else {
                                    showPopup(response.message);
                                    return false;
                                }
                            })
                            .error(function(error) {
                                console.log("error", error);
                            });
                    }
                };
            } else {
                showPopup("예약이 취소가 불가능한 상태입니다.<br/>관리자에게 문의하세요.");
                return;
            }
        };

        $scope.addFavoriteShop = function() {
            if (!isLogin()) {
                showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        location.href = "#/login?redirectUrl=#/shop_view/" + $scope.shopId;
                    }
                };

                return;
            }

            var params = {};
            params.apiType = "add_favorite_shop";
            params.token = getStorageData("token");
            params.shopId = $scope.orderDetail.SHOP_ID;

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        showPopup("찜 목록에 등록 되었습니다.");
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('MyHistoryDetailController', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.orderId = $stateParams.orderId;
        $scope.orderDetail = {};

        var params = {};
        params.apiType = "booking_detail";
        params.orderId = $scope.orderId;
        params.token = getStorageData("token");

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.orderDetail = response.data;
                        $scope.orderDetail.bookingMonthDate = $scope.orderDetail.ORDER_DT.substr(0, 4) + "." + $scope.orderDetail.ORDER_DT.substr(4, 2) + "." + $scope.orderDetail.ORDER_DT.substr(6, 2);

                        var startTime = new Date(($scope.orderDetail.ORDER_DT.substr(0, 4) * 1), ($scope.orderDetail.ORDER_DT.substr(4, 2) * 1) - 1, ($scope.orderDetail.ORDER_DT.substr(6, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(8, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(10, 2) * 1), ($scope.orderDetail.ORDER_DT.substr(12, 2) * 1));
                        var endTime = new Date(startTime.getTime() + ($scope.orderDetail.ELAPSE_TM * 60 * 1000));

                        $scope.orderDetail.startTimeStr = (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) + ":" + (startTime.getMinutes() < 10 ? '0' + startTime.getMinutes() : startTime.getMinutes());
                        $scope.orderDetail.endTimeStr = (endTime.getHours() < 10 ? '0' + endTime.getHours() : endTime.getHours()) + ":" + (endTime.getMinutes() < 10 ? '0' + endTime.getMinutes() : endTime.getMinutes());
                    }
                } else {
                    showPopup(response.message);
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.$on('$ionicView.enter', function() {

        });

        $scope.goToReview = function() {
            $ionicHistory.clearCache().then(function() {
                goToShopReviewPage($scope.orderDetail.SHOP_ID, $scope.orderDetail.SHOP_NM);
            });
        };

        $scope.goToReservation = function() {
            $ionicHistory.clearCache().then(function() {
                goToBooking1Page($scope.orderDetail.SHOP_ID);
            });
        };

        $scope.addFavoriteShop = function() {
            if (!isLogin()) {
                showConfirmPopup("로그인이 필요한 기능입니다.<br/>로그인 후 이용하시겠습니까?");
                popupConfirmCallback = function(_ret) {
                    if (_ret) {
                        location.href = "#/login?redirectUrl=#/shop_view/" + $scope.shopId;
                    }
                };

                return;
            }

            var params = {};
            params.apiType = "add_favorite_shop";
            params.token = getStorageData("token");
            params.shopId = $scope.orderDetail.SHOP_ID;

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        showPopup("찜 목록에 등록 되었습니다.");
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('MemberJoinController', function($scope, $http, $location, $ionicHistory) {
        $scope.phoneAuthData = {
            phoneAuthToken: new Date().getTime(),
            phoneAuthIntervalId: 0
        };

        $scope.joinData = {
            memberAccountId: "",
            checkedMemberAccountId: "",
            ynCheckDuplicateId: "N",
            memberAccountPw: "",
            memberAccountPwConfirm: "",
            email: "",
            nickName: "",
            phoneNumber: "",
            phoneNumberAuth: "",
            ynPhoneNumberAuth: "N",
            userRealNm: "",
            userCi: "",
            userDi: "",
            policy_1: "",
            policy_2: "",
            policy_3: ""
        };

        $scope.$on('$ionicView.enter', function() {
            $scope.joinData = {
                memberAccountId: "",
                checkedMemberAccountId: "",
                ynCheckDuplicateId: "N",
                memberAccountPw: "",
                memberAccountPwConfirm: "",
                email: "",
                nickName: "",
                phoneNumber: "",
                phoneNumberAuth: "",
                ynPhoneNumberAuth: "N",
                userRealNm: "",
                userCi: "",
                userDi: "",
                policy_1: "",
                policy_2: "",
                policy_3: ""
            };
        });

        $scope.checkDulicateId = function() {
            if (!$scope.checkId()) {
                return false;
            }

            var params = {};
            params.apiType = "check_duplicate_id";
            params.memberAccountId = $scope.joinData.memberAccountId;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));
                    if (!response) {
                        showPopup("알 수 없는 오류입니다.");
                        return false;
                    }

                    if (response.result == '100') {
                        showPopup("사용 가능한 아이디입니다.");

                        $scope.joinData.checkedMemberAccountId = $scope.joinData.memberAccountId;
                        $scope.joinData.ynCheckDuplicateId = "Y";

                        return true;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                    return false;
                });
        };

        $scope.agreePolicy = function(_index) {
            if ($('.agreement > li:eq(' + _index + ') > span').children("i").hasClass("ico_check")) {
                $('.agreement > li:eq(' + _index + ') > span').children("i").removeClass("ico_check");
                $('.agreement > li:eq(' + _index + ') > span').children("i").addClass("ico_check_pink");
            } else if ($('.agreement > li:eq(' + _index + ') > span').children("i").hasClass("ico_check_pink")) {
                $('.agreement > li:eq(' + _index + ') > span').children("i").removeClass("ico_check_pink");
                $('.agreement > li:eq(' + _index + ') > span').children("i").addClass("ico_check");
            }
        };

        $scope.checkId = function() {
            if (!$scope.joinData.memberAccountId || $scope.joinData.memberAccountId.length <= 0) {
                showPopup("아이디를 입력해주세요.");
                return false;
            }

            if ($scope.joinData.memberAccountId.length < 5 || $scope.joinData.memberAccountId.length > 20) {
                showPopup("아이디는 5자이상 20자이하로 입력해주세요.");
                return false;
            }

            if (!checkValidId($scope.joinData.memberAccountId)) {
                showPopup("아이디는 5~20자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다.");
                return false;
            }

            return true;
        };

        $scope.checkPw = function() {
            if (!$scope.joinData.memberAccountPw || $scope.joinData.memberAccountPw.length <= 0) {
                showPopup("비밀번호를 입력해주세요.");
                return false;
            }

            if ($scope.joinData.memberAccountPwConfirm != $scope.joinData.memberAccountPw) {
                showPopup("비밀번호를 다시 확인해주세요.");
                return false;
            }

            if (!checkValidPw($scope.joinData.memberAccountPw)) {
                showPopup("비밀번호는 6~20자 영문 소문자, 숫자가 포함되어야 합니다.");
                return false;
            }

            return true;
        };

        $scope.checkEmail = function() {
            if (!$scope.joinData.email || $scope.joinData.email.length <= 0) {
                showPopup("이메일 주소를 입력해주세요.");
                return false;
            }

            if (!checkValidEmail($scope.joinData.email)) {
                showPopup("유효한 이메일 주소를 입력해주세요.");
                return false;
            }

            return true;
        };

        $scope.checkNickName = function() {
            if (!$scope.joinData.nickName || $scope.joinData.nickName.length <= 0) {
                showPopup("닉네임을 입력해주세요.");
                return false;
            }

            return true;
        };

        $scope.checkPhoneNumber = function() {
            if (!$scope.joinData.phoneNumber || $scope.joinData.phoneNumber.length <= 0) {
                showPopup("휴대폰 번호를 입력해주세요.");
                return false;
            }

            return true;
        };

        $scope.showConfirmPhoneNumber = function() {
            $scope.phoneAuthData.phoneAuthToken = new Date().getTime();
            $("#confirmPhoneNumberFrame").attr("src", IFRME_SERVER_HOST + "/okname/confirm_1.php?phoneAuthToken=" + $scope.phoneAuthData.phoneAuthToken);

            $("#layer_phone_confirm").show();

            $scope.phoneAuthData.phoneAuthIntervalId = setInterval(function() {
                $scope.getTempPhoneAuth();
            }, 1000);
        };

        $scope.getTempPhoneAuth = function() {
            var params = {};
            params.apiType = "get_temp_phone_auth";
            params.phoneAuthToken = $scope.phoneAuthData.phoneAuthToken;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            var phoneAuthData = response.data;

                            $scope.joinData.phoneNumber = phoneAuthData.USER_PHONE1;
                            $scope.joinData.ynPhoneNumberAuth = 'Y';
                            $scope.joinData.userRealNm = phoneAuthData.USER_REAL_NM;
                            $scope.joinData.userCi = phoneAuthData.USER_CI;
                            $scope.joinData.userDi = phoneAuthData.USER_DI;

                            $scope.hideConfirmPhoneNumber();
                        }
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.hideConfirmPhoneNumber = function() {
            $("#layer_phone_confirm").hide();

            if ($scope.phoneAuthData.phoneAuthIntervalId > 0) {
                clearInterval($scope.phoneAuthData.phoneAuthIntervalId);
            }
        };

        $scope.checkPolicy = function() {
            var isAllChecked = true;

            for (var i = 0; i < 3; i++) {
                if ($('.agreement > li:eq(' + i + ') > span').children("i").hasClass("ico_check")) {
                    isAllChecked = false;
                    break;
                }
            }

            if (!isAllChecked) {
                showPopup("약관에 모두 동의를 해주셔야 가입이 가능합니다.");
                return false;
            }

            return true;
        };

        $scope.joinMember = function() {
            // 아이디 체크
            if (!$scope.checkId()) {
                return false;
            }

            if ($scope.joinData.ynCheckDuplicateId == "N" || $scope.joinData.checkedMemberAccountId != $scope.joinData.memberAccountId) {
                showPopup("아이디 중복체크를 해주세요.");
                return false;
            }

            // 비밀번호 체크
            if (!$scope.checkPw()) {
                return false;
            }

            // 이메일 체크 
            if (!$scope.checkEmail()) {
                return false;
            }

            // 닉네임 체크 
            if (!$scope.checkNickName()) {
                return false;
            }

            // 휴대폰 번호 체크
            if (!$scope.checkPhoneNumber()) {
                return false;
            }

            if ($scope.joinData.ynPhoneNumberAuth != "Y") {
                showPopup("휴대폰 본인인증을 해주세요.");
                return false;
            }

            // 이용약관 체크
            if (!$scope.checkPolicy()) {
                return false;
            }

            var params = {};
            params.apiType = "member_join";
            params.memberAccountId = $scope.joinData.memberAccountId;
            params.memberAccountPw = $scope.joinData.memberAccountPw;
            params.email = $scope.joinData.email;
            params.nickName = $scope.joinData.nickName;
            params.userRealNm = $scope.joinData.userRealNm;
            params.phoneNumber = $scope.joinData.phoneNumber;
            params.userCi = $scope.joinData.userCi;
            params.userDi = $scope.joinData.userDi;
            params.memberTpCd = "003";
            params.gcmId = gcmId;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        showPopup("회원가입이 완료 되었습니다.");

                        popupCallback = function() {
                            $ionicHistory.clearCache().then(function() {
                                location.replace('#/login');
                            });
                        };
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('LoginController', function($scope, $http, $location, $ionicHistory) {
        $scope.$on('$ionicView.enter', function() {
            var ynAutoLogin = getStorageData("autoLogin");
            if (ynAutoLogin == "Y") {
                $("#login_chk").attr("checked", "true");
            } else {
                $("#login_chk").attr("checked", "false");
            }
        });

        $scope.memberAccountId = "";
        $scope.memberAccountPw = "";
        $scope.login = function() {
            if ($scope.memberAccountId.length <= 0) {
                showPopup("아이디를 입력해주세요.");
                return;
            }

            if ($scope.memberAccountPw.length <= 0) {
                showPopup("비밀번호를 입력해주세요.");
                return;
            }

            var deviceInfo = ionic.Platform.device();

            var params = {};
            params.apiType = "login";
            params.memberAccountId = $scope.memberAccountId;
            params.memberAccountPw = $scope.memberAccountPw;
            params.platform = deviceInfo.platform;
            params.platformVersion = deviceInfo.version;

            console.log("params : " + JSON.stringify(params));

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.memberAccountId = "";
                        $scope.memberAccountPw = "";

                        // response : {"result":"100","message":"SUCCESS","data":{"token":"$2y$11$2ZeGHJ0mXWidblwrK01hD.IlKArsHYHmW1W8424jKTCgEmEWOnuhC","memberInfo":{"USER_ID":26,"USER_NM":"testtest","USER_DESC":"사용자","USER_EMAIL":"asdfas@asdf.asdf","USER_TP_CD":"003","USER_ACCOUNT_ID":"testtest","USER_PHONE1":"1232131","PHONE_CERT_YN":"Y"}}}
                        setStorageData("memberInfo", response.data.memberInfo);
                        setStorageData("token", response.data.token);

                        if ($("#login_chk").attr("checked")) {
                            setStorageData("autoLogin", "Y");
                        } else {
                            setStorageData("autoLogin", "N");
                        }

                        getGcmIdAndUpdate();

                        $ionicHistory.clearCache().then(function() {
                            location.replace('#/main');
                        });
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('ShopFavoriteController', function($scope, $http, $ionicHistory) {
        $scope.currentTab = 0;
        $scope.favoriteShopList = [];
        $scope.couponList = [];

        $scope.$on('$ionicView.enter', function() {
            if (isLogin()) {
                var memberInfo = getStorageData("memberInfo");
                $(".hello_master > em").text(memberInfo.USER_NM + "님");

                $("#stateLogout").hide();
                $("#stateLogin").show();
            } else {
                $("#stateLogout").show();
                $("#stateLogin").hide();
            }

            $("#dim").off("click").on("click", function() {
                closeNav();
            });

            $scope.changeFavoriteShopTab($scope.currentTab);
        });

        $scope.openNav = function() {
            openNav();
        };

        $scope.goToShopDetailPage = function(_shopId) {
            $ionicHistory.clearCache().then(function() {
                goToShopDetailPage(_shopId);
            });
        };

        $scope.goToReservation = function(_shopId) {
            $ionicHistory.clearCache().then(function() {
                goToBooking1Page(_shopId);
            });
        };

        $scope.changeFavoriteShopTab = function(_idx) {
            $scope.currentTab = _idx;

            $("#favoriteShopTab > span").each(function(key, val) {
                $(this).removeClass("on");
                $('.favoriteShopTab0' + key).hide();
            });

            $("#favoriteShopTab > span:eq(" + _idx + ")").addClass("on");
            $('.favoriteShopTab0' + _idx).show();

            var params = {};
            params.token = getStorageData("token");

            if ($scope.currentTab === 0) {
                params.apiType = "favorite_shop_list";
            } else {
                params.apiType = "coupon_list";
            }

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            if ($scope.currentTab === 0) {
                                $scope.favoriteShopList = response.data;

                                $.each($scope.favoriteShopList, function(_key, _val) {
                                    if (_val.SHOP_IMG1) {
                                        _val.SHOP_IMG1 = IMAGE_SERVER_HOST + _val.SHOP_IMG1;
                                    }
                                });
                            } else {
                                $scope.couponList = response.data;
                            }
                        }
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.goToHome = function() {
            $ionicHistory.clearCache().then(function() {
                goToHome();
            });
        };

        $scope.goToNearByShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToNearByShopPage();
            });
        };

        $scope.goToFavoriteShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToFavoriteShopPage();
            });
        };

        $scope.goToMyNailPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToMyNailPage();
            });
        };

        $scope.goToLocationShopListPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToLocationShopListPage();
            });
        };
    })
    .controller('SearchIdController', function($scope, $http, $ionicHistory) {
        $scope.phoneAuthToken = new Date().getTime();
        $scope.phoneAuthIntervalId = 0;
        $scope.data = {
            email: "",
            phoneNumber: "",
            phoneNumberAuthYN: "N"
        };

        $scope.showConfirmPhoneNumber = function() {
            $scope.phoneAuthToken = new Date().getTime();
            $("#confirmPhoneNumberSearchIdFrame").attr("src", IFRME_SERVER_HOST + "/okname/confirm_1.php?phoneAuthToken=" + $scope.phoneAuthToken);

            $("#layer_phone_confirm_search_id").show();

            $scope.phoneAuthIntervalId = setInterval(function() {
                $scope.getTempPhoneAuth();
            }, 1000);
        };

        $scope.getTempPhoneAuth = function() {
            var params = {};
            params.apiType = "get_temp_phone_auth";
            params.phoneAuthToken = $scope.phoneAuthToken;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            var phoneAuthData = response.data;

                            $scope.data.phoneNumber = phoneAuthData.USER_PHONE1;
                            $scope.data.phoneNumberAuthYN = "Y";

                            $scope.hideConfirmPhoneNumber();
                        }
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.hideConfirmPhoneNumber = function() {
            $("#layer_phone_confirm_search_id").hide();

            if ($scope.phoneAuthIntervalId > 0) {
                clearInterval($scope.phoneAuthIntervalId);
            }
        };

        $scope.searchId = function() {
            if ($scope.data.email.length <= 0) {
                showPopup("이메일 주소를 입력해주세요.");
                return false;
            }

            if (!checkValidEmail($scope.data.email)) {
                showPopup("유효한 이메일 주소를 입력해주세요.");
                return false;
            }

            if ($scope.data.phoneNumber.length <= 0) {
                showPopup("휴대폰 번호를 입력해주세요.");
                return false;
            }

            if ($scope.data.phoneNumberAuthYN != "Y") {
                showPopup("휴대폰 본인인증을 진행해주세요.");
                return false;
            }

            var params = {};
            params.apiType = "searchId";
            params.email = $scope.data.email;
            params.phoneNumber = $scope.data.phoneNumber;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.data.email = "";
                        $scope.data.phoneNumber = "";
                        $scope.data.phoneNumberAuth = "";

                        showPopup("회원님의 아이디는<br/><em>" + response.data.USER_ACCOUNT_ID + "</em>입니다.");
                        popupCallback = function() {
                            $ionicHistory.clearCache().then(function() {
                                location.replace('#/login');
                            });
                        };
                    } else {
                        showPopup(response.message);
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('SearchPWController', function($scope, $http, $ionicHistory) {
        $scope.phoneAuthToken = new Date().getTime();
        $scope.phoneAuthIntervalId = 0;

        $scope.data = {
            memberAccountId: "",
            phoneNumber: "",
            phoneNumberAuthYN: "N"
        };

        $scope.showConfirmPhoneNumber = function() {
            $scope.phoneAuthToken = new Date().getTime();
            $("#confirmPhoneNumberSearchPwFrame").attr("src", IFRME_SERVER_HOST + "/okname/confirm_1.php?phoneAuthToken=" + $scope.phoneAuthToken);

            $("#layer_phone_confirm_search_pw").show();

            $scope.phoneAuthIntervalId = setInterval(function() {
                $scope.getTempPhoneAuth();
            }, 1000);
        };

        $scope.getTempPhoneAuth = function() {
            var params = {};
            params.apiType = "get_temp_phone_auth";
            params.phoneAuthToken = $scope.phoneAuthToken;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        if (response.data) {
                            var phoneAuthData = response.data;

                            $scope.data.phoneNumber = phoneAuthData.USER_PHONE1;
                            $scope.data.phoneNumberAuthYN = "Y";

                            $scope.hideConfirmPhoneNumber();
                        }
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.hideConfirmPhoneNumber = function() {
            $("#layer_phone_confirm_search_pw").hide();

            if ($scope.phoneAuthIntervalId > 0) {
                clearInterval($scope.phoneAuthIntervalId);
            }
        };

        $scope.searchPassword = function() {
            if ($scope.data.memberAccountId.length <= 0) {
                showPopup("아이디를 입력해주세요.");
                return false;
            }

            if ($scope.data.phoneNumber.length <= 0) {
                showPopup("휴대폰 번호를 입력해주세요.");
                return false;
            }

            if ($scope.data.phoneNumberAuthYN != "Y") {
                showPopup("휴대폰 본인인증을 진행해주세요.");
                return false;
            }

            var params = {};
            params.apiType = "searchPw";
            params.memberAccountId = $scope.data.memberAccountId;
            params.phoneNumber = $scope.data.phoneNumber;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.data.memberAccountId = "";
                        $scope.data.phoneNumber = "";
                        $scope.data.phoneNumberAuth = "";

                        location.href = "#/search_pw2?memberId=" + response.data.USER_ID + "&phoneNumber=" + response.data.USER_PHONE1;
                    } else {
                        showPopup(response.message);
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('SearchPW2Controller', function($scope, $location, $http, $ionicHistory) {
        $scope.memberId = $location.search().memberId;
        $scope.phoneNumber = $location.search().phoneNumber;
        $scope.memberAccountPwd = "";
        $scope.memberAccountPwdConfirm = "";

        $scope.savePassword = function() {
            if ($scope.memberAccountPwd.length <= 0) {
                showPopup("비밀번호를 입력해주세요.");
                return;
            }

            if ($scope.memberAccountPwd != $scope.memberAccountPwdConfirm) {
                showPopup("비밀번호가 일치하지 않습니다.<br/>비밀번호를 다시 확인해주세요.");
                return;
            }

            var params = {};
            params.apiType = "savePassword";
            params.memberId = $scope.memberId;
            params.phoneNumber = $scope.phoneNumber;
            params.memberAccountPwd = $scope.memberAccountPwd;

            $http({ method: "POST", url: SERVER_HOST_API + "/member.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.memberAccountPwd = "";
                        $scope.memberAccountPwdConfirm = "";

                        showPopup("비밀번호가 변경 되었습니다.<br/>다시 로그인 해 주세요.");
                        popupCallback = function() {
                            $ionicHistory.clearCache().then(function() {
                                location.replace('#/login');
                            });
                        };
                    } else {
                        showPopup(response.message);
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('WriteReviewController', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.shopId = $stateParams.shopId;
        $scope.shopNm = $stateParams.shopNm;
        $scope.title = "";
        $scope.content = "";

        $scope.$on('$ionicView.enter', function() {
            $(".grade_star > li").off("click").on("click", function() {
                var index = $(this).attr("data-index");

                $(".grade_star > li").each(function(key, val) {
                    if (key < index) {
                        $(this).addClass("on");
                    } else {
                        $(this).removeClass("on");
                    }
                });
            });
        });

        $scope.saveReview = function() {
            var starCount = 0;
            $(".grade_star > li").each(function(key, val) {
                if ($(this).hasClass("on")) {
                    starCount++;
                }
            });

            if (starCount === 0) {
                showPopup("별점을 선택해주세요.");
                return;
            }

            if ($scope.title.length <= 0) {
                showPopup("후기 제목을 입력해주세요.");
                return;
            }

            if ($scope.content.length <= 0) {
                showPopup("후기 내용을 입력해주세요.");
                return;
            }

            var params = {};
            params.apiType = "save_review";
            params.token = getStorageData("token");
            params.starCount = starCount;
            params.shopId = $scope.shopId;
            params.title = $scope.title;
            params.content = $scope.content;

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.title = "";
                        $scope.content = "";

                        showPopup("후기가 등록 되었습니다.");
                        popupCallback = function() {
                            $ionicHistory.clearCache().then(function() {
                                location.replace('#/shop_view/' + $scope.shopId);
                            });
                        };
                    } else if (response.result == "300") {
                        removeAllMemberData();

                        showPopup(response.message);
                        popupCallback = function() {
                            location.replace('#/login');
                        };
                        return false;
                    } else {
                        showPopup(response.message);
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };
    })
    .controller('EventListController', function($scope, $http, $stateParams, $ionicHistory) {
        $scope.evtId = $stateParams.evtId;
        $scope.eventDetail = {};
        $scope.shopList = [];
        $scope.event_list = [];

        $scope.$on('$ionicView.enter', function() {
            $scope.swiper = new Swiper('#event_list_event_banner', {
                pagination: '.swiper-pagination',
                paginationClickable: true,
                spaceBetween: 30,
            });

            var topBarOffset = $("#fixNextTagEvent").offset();

            $(".eventListScrollView").scroll(function() {
                var docScrollY = $(".eventListScrollView").scrollTop();

                if (docScrollY >= topBarOffset.top) {
                    $("#eventTopBar").show();
                } else {
                    $("#eventTopBar").hide();
                }
            });

            $scope.getEventList();
        });

        $scope.getEventList = function() {
            var params = {};
            params.apiType = "event_list_all";

            $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
                .success(function(response) {
                    console.log("response : " + JSON.stringify(response));

                    if (response.result == "100") {
                        $scope.event_list = response.data;

                        $scope.drawEventList();
                    } else {
                        showPopup(response.message);
                        return false;
                    }
                })
                .error(function(error) {
                    console.log("error", error);
                });
        };

        $scope.drawEventList = function() {
            $.each($scope.event_list, function(_key, _val) {
                $("#event_list_event_banner > .swiper-wrapper").append("<div class='swiper-slide'><img src='" + IMAGE_SERVER_HOST + _val.EVT_BAN_IMG + "'/></div>");
            });

            $scope.swiper.init();
        };

        $scope.goToShopDetailPage = function(_shopId) {
            $ionicHistory.clearCache().then(function() {
                goToShopDetailPage(_shopId);
            });
        };

        var params = {};
        params.apiType = "shop_list_by_event";
        params.evtId = $scope.evtId;

        $http({ method: "POST", url: SERVER_HOST_API + "/shop.php", data: params })
            .success(function(response) {
                console.log("response : " + JSON.stringify(response));

                if (response.result == "100") {
                    if (response.data) {
                        $scope.eventDetail = response.data.eventDetail;
                        $scope.shopList = response.data.shopList;

                        $.each($scope.shopList, function(_key, _val) {
                            if (_val.SHOP_IMG1) {
                                _val.SHOP_IMG1 = IMAGE_SERVER_HOST + _val.SHOP_IMG1;
                            }
                        });
                    }
                } else {
                    showPopup(response.message);
                    return false;
                }
            })
            .error(function(error) {
                console.log("error", error);
            });

        $scope.goToHome = function() {
            $ionicHistory.clearCache().then(function() {
                goToHome();
            });
        };

        $scope.goToNearByShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToNearByShopPage();
            });
        };

        $scope.goToFavoriteShopPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToFavoriteShopPage();
            });
        };

        $scope.goToMyNailPage = function() {
            $ionicHistory.clearCache().then(function() {
                goToMyNailPage();
            });
        };
    });