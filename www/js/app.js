angular.module('BinjariApp', ['ionic', 'BinjariApp.controllers', 'ngCordova', 'ion-sticky', 'ionic-datepicker'])
    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            }

            if (window.StatusBar) {
                StatusBar.styleDefault();
                StatusBar.overlaysWebView(false);
            }

            if (window.FCMPlugin) {
                FCMPlugin.getToken(function(_token) {
                    gcmId = _token;
                });

                FCMPlugin.onTokenRefresh(function(_token) {
                    gcmId = _token;

                    updateGcmId(gcmId);
                });

                FCMPlugin.onNotification(function(_data) {
                    console.log("onNotification() : " + JSON.stringify("_data"));
                    if (_data) {
                        parseNotification(_data);
                    }
                });
            }
        });
    })
    .filter('split', function() {
        return function(input, delimiter) {
            delimiter = delimiter || ',';

            if (!input || !input.length) return;

            return input.split(delimiter);
        };
    })
    .filter('operationTime', function() {
        return function(time) {
            var hours = Math.floor(time / 60);
            var minutes = time % 60;
            var retTime = "";

            if (hours >= 1) {
                retTime += hours + "시간 ";
            }

            if (minutes > 0) {
                retTime += minutes + "분";
            }

            return retTime;
        };
    })
    .filter('bookingDate', function() {
        return function(_date, _elapsedTime) {
            if (!_date || !_date.length) return;

            var startDate = new Date((_date.substr(0, 4) * 1), (_date.substr(4, 2) * 1) - 1, (_date.substr(6, 2) * 1), (_date.substr(8, 2) * 1), (_date.substr(10, 2) * 1), (_date.substr(12, 2) * 1));
            var endDate = new Date(startDate.getTime() + (_elapsedTime * 60 * 1000));

            var bookingDateTime = startDate.getFullYear();
            bookingDateTime += "." + ((startDate.getMonth() + 1) < 10 ? '0' + (startDate.getMonth() + 1) : (startDate.getMonth() + 1));
            bookingDateTime += "." + (startDate.getDate() < 10 ? '0' + startDate.getDate() : startDate.getDate());
            bookingDateTime += " " + (startDate.getHours() < 10 ? '0' + startDate.getHours() : startDate.getHours());
            bookingDateTime += ":" + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes());
            bookingDateTime += " ~ " + (endDate.getHours() < 10 ? '0' + endDate.getHours() : endDate.getHours());
            bookingDateTime += ":" + (endDate.getMinutes() < 10 ? '0' + endDate.getMinutes() : endDate.getMinutes());

            return bookingDateTime;
        };
    })
    .filter('orderDt', function() {
        return function(_date) {
            if (!_date || !_date.length) return;

            var startDate = new Date((_date.substr(0, 4) * 1), (_date.substr(4, 2) * 1) - 1, (_date.substr(6, 2) * 1), (_date.substr(8, 2) * 1), (_date.substr(10, 2) * 1), (_date.substr(12, 2) * 1));

            var orderDateTime = startDate.getFullYear();
            orderDateTime += "." + ((startDate.getMonth() + 1) < 10 ? '0' + (startDate.getMonth() + 1) : (startDate.getMonth() + 1));
            orderDateTime += "." + (startDate.getDate() < 10 ? '0' + startDate.getDate() : startDate.getDate());
            orderDateTime += " " + (startDate.getHours() < 10 ? '0' + startDate.getHours() : startDate.getHours());
            orderDateTime += ":" + (startDate.getMinutes() < 10 ? '0' + startDate.getMinutes() : startDate.getMinutes());
            orderDateTime += ":" + (startDate.getSeconds() < 10 ? '0' + startDate.getSeconds() : startDate.getSeconds());

            return orderDateTime;
        };
    })
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/main');
        $stateProvider

            .state('main', {
            url: '/main',
            templateUrl: 'templates/main.html',
            controller: 'MainController'
        })

        .state('shop_list', {
            url: '/shop_list',
            templateUrl: 'templates/shops/shop_list.html',
            controller: 'ShopListController'
        })

        .state('location_shop_list', {
            url: '/location_shop_list',
            templateUrl: 'templates/shops/location_shoplist.html',
            controller: 'LocationShopListController'
        })

        .state('location_select2', {
            url: '/location_select2',
            templateUrl: 'templates/shops/location_select_2.html',
            controller: 'LocationSelect2Controller'
        })

        .state('location_select', {
            url: '/location_select',
            templateUrl: 'templates/shops/location_select.html',
            controller: 'LocationSelectController'
        })

        .state('event_list', {
            url: '/event_list/:evtId',
            templateUrl: 'templates/shops/event_list.html',
            controller: 'EventListController'
        })

        .state('shop_view', {
            url: '/shop_view/:shopId',
            templateUrl: 'templates/shops/shop_view.html',
            controller: 'ShopViewController'
        })

        .state('shop_write_review', {
            url: '/shop_write_review/:shopId/:shopNm',
            templateUrl: 'templates/shops/write_review.html',
            controller: 'WriteReviewController'
        })

        .state('shop_portfolio_1', {
            url: '/shop_portfolio_1/:shopId',
            templateUrl: 'templates/shops/portfolio_1.html',
            controller: 'ShopPortfolio1Controller'
        })

        .state('shop_portfolio_2', {
            url: '/shop_portfolio_2/:portId',
            templateUrl: 'templates/shops/portfolio_2.html',
            controller: 'ShopPortfolio2Controller'
        })

        .state('shop_portfolio_3', {
            url: '/shop_portfolio_3/:shopId/:tagNm',
            templateUrl: 'templates/shops/portfolio_3.html',
            controller: 'ShopPortfolio3Controller'
        })

        .state('shop_booking_1', {
            url: '/shop_booking_1/:shopId',
            templateUrl: 'templates/booking/booking_1.html',
            controller: 'ShopBooking1Controller'
        })

        .state('shop_booking_2', {
            url: '/shop_booking_2',
            templateUrl: 'templates/booking/booking_2.html',
            controller: 'ShopBooking2Controller'
        })

        .state('shop_booking_3', {
            url: '/shop_booking_3/:orderId',
            templateUrl: 'templates/booking/booking_3.html',
            controller: 'ShopBooking3Controller'
        })

        .state('shop_schedule_1', {
            url: '/shop_schedule_1/:shopId',
            templateUrl: 'templates/booking/schedule_1.html',
            controller: 'ShopSchedule1Controller'
        })

        .state('shop_schedule_2', {
            url: '/shop_schedule_2/:shopId',
            templateUrl: 'templates/booking/schedule_2.html',
            controller: 'ShopSchedule2Controller'
        })

        .state('login', {
            url: '/login',
            templateUrl: 'templates/member/login.html',
            controller: 'LoginController'
        })

        .state('member_join', {
            url: '/member_join',
            templateUrl: 'templates/member/join.html',
            controller: 'MemberJoinController'
        })

        .state('agreement', {
            url: '/agreement',
            templateUrl: 'templates/member/agreement.html'
        })

        .state('search_id', {
            url: '/search_id',
            templateUrl: 'templates/member/search_id.html',
            controller: 'SearchIdController'
        })

        .state('search_pw', {
            url: '/search_pw',
            templateUrl: 'templates/member/search_pw.html',
            controller: 'SearchPWController'
        })

        .state('search_pw2', {
            url: '/search_pw2',
            templateUrl: 'templates/member/search_pw_2.html',
            controller: 'SearchPW2Controller'
        })

        .state('my_nail', {
            url: '/my_nail',
            templateUrl: 'templates/mypage/mynail.html',
            controller: 'MyNailController'
        })

        .state('my_reserved', {
            url: '/my_reserved/:orderId',
            templateUrl: 'templates/mypage/reserved.html',
            controller: 'MyBookingDetailController'
        })

        .state('my_history_detail', {
            url: '/history_detail/:orderId',
            templateUrl: 'templates/mypage/history_detail.html',
            controller: 'MyHistoryDetailController'
        })

        .state('shop_favorite', {
            url: '/shop_favorite',
            templateUrl: 'templates/mypage/shop_favorite.html',
            controller: "ShopFavoriteController"
        })

        .state('coupon', {
            url: '/coupon/:shopId',
            templateUrl: 'templates/booking/coupon.html',
            controller: 'ShopCouponListController'
        });
    });