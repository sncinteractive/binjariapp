function parseNotification(_data) {
    console.log(JSON.stringify(_data));

    showNotificationPopup(_data);

    if (_data.wasTapped) {

    } else {

    }
}

function showNotificationPopup(_data) {
    var content = getNotificationContent(_data);
    if (!content) {
        return;
    }

    $(".notificationContent").empty();
    $(".notificationContent").html(content);

    $("#layerNotification").show();

    $("#notificationOK").off("click").on("click", function() {
        $("#layerNotification").hide();

        if (_data.messageType == "shopBooking") {
            location.href = "#/main";
        } else if (_data.messageType == "sendCoupon") {
            isPopupShowing();
            goToFavoriteShopPage();
        }
    });

    $("#notificationClose").off("click").on("click", function() {
        $("#layerNotification").hide();
    });
}

function getNotificationContent(_data) {
    var contentHtml = "";

    if (_data.messageType == "booking") {
        contentHtml += _data.customerName + "고객님</br>";
        contentHtml += parseBookingDate(_data.bookingDate) + "</br>";
        contentHtml += _data.productName + "상품으로 예약되었습니다.</br></br>";
        contentHtml += "시술시간 " + parseElapsedTime(_data.elapsedTime) + " 소요예정</br></br>";
    } else if (_data.messageType == "sendCoupon") {
        contentHtml += _data.customerName + "고객님</br>";
        contentHtml += _data.shopName + " 샵에서</br>";
        contentHtml += "쿠폰이 발행 되었습니다.</br>";
        contentHtml += "확인 부탁드립니다.</br></br>";
    }

    return contentHtml;
}

function parseBookingDate(bookingDate) {
    var startTime = new Date(bookingDate.substr(0, 4), (parseInt(bookingDate.substr(4, 2), 10) - 1), bookingDate.substr(6, 2), bookingDate.substr(8, 2), bookingDate.substr(10, 2), bookingDate.substr(12, 2));

    var startTimeStr = "";
    startTimeStr += startTime.getFullYear() + "년 ";
    startTimeStr += ((startTime.getMonth() + 1) < 10 ? '0' + (startTime.getMonth() + 1) : (startTime.getMonth() + 1)) + "월 ";
    startTimeStr += (startTime.getDate() < 10 ? '0' + startTime.getDate() : startTime.getDate()) + "일 ";
    startTimeStr += (startTime.getHours() < 10 ? '0' + startTime.getHours() : startTime.getHours()) + "시";

    return startTimeStr;
}

function parseElapsedTime(time) {
    var hours = time / 60;
    var minutes = time % 60;
    var retTime = "";

    if (hours >= 1) {
        retTime += parseInt(hours, 10) + "시간 ";
    }

    if (minutes > 0) {
        retTime += parseInt(minutes) + "분";
    }

    return retTime;
}